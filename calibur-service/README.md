# 配置文件


spring:
  # 数据库驱动：
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    # 数据源名称
    name: defaultDataSource
    # 数据库连接地址
    url: jdbc:mysql://localhost:3306/security_jwt_test?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf-8&useSSL=true
    # 数据库用户名&密码：
    username: root
    password: root

  # Redis配置
  redis:
    # Redis数据库索引（默认为0）
    database: 1
    # Redis服务器地址
    host: localhost
    # Redis服务器连接端口
    port: 6379
    # Redis服务器连接密码（默认为空）
    password:
    # 连接超时时间（毫秒）
    timeout: 5000
    pool:
      # 连接池最大连接数（使用负值表示没有限制）
      max-active: 200
      # 连接池最大阻塞等待时间（使用负值表示没有限制）
      max-wait: 1
      # 连接池中的最大空闲连接
      max-idle: 10
      # 连接池中的最小空闲连接
      min-idle: 0

## mybatis-plus 配置
mybatis-plus:
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
  global-config:
    db-config:
      ## 驼峰
      table-underline: true
      ## 逻辑删除 默认 0未删除 1删除
      logic-delete-field: daleted
  type-aliases-package: com.wyz.calibur.pojo
  mapper-locations: 
   - com/wyz/calibur/infrastructure/db/*/xml/*.xml
## 逻辑删除注解  @TableLogic 注解，用于触发拦截器，删除时不会对其进行删除，执行的是更新操作，把deleted字段改为了deleted=1，表示已经删除
## @TableField(fill = FieldFill.INSERT)，当添加时，会自动给该属性赋值，具体赋值什么就要看我们在配置类中的配置了，这里默认添加deleted=0

## 白名单URL
secure:
  ignored:
    url:
      - /swagger-ui
      - /v3/api-doc
      - /swagger-resources/**
      - /webjars/**
      - /v2/**
      - /swagger
      - /swagger-ui.html/**
      - /doc.html
      - /login
      - /logout
      - /success
      - /error
      - /index
security:
  basic:
    enabled: false




wyz:
  username: weiyaozhong
