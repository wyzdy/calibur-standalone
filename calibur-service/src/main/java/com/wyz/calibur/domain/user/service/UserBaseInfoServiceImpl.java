package com.wyz.calibur.domain.user.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wyz.calibur.infrastructure.db.user.mapper.UserBaseInfoMapper;
import com.wyz.calibur.pojo.user.UserBaseInfo;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户基础信息表 服务实现类
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
@Service
public class UserBaseInfoServiceImpl extends ServiceImpl<UserBaseInfoMapper, UserBaseInfo> implements UserBaseInfoService {


}
