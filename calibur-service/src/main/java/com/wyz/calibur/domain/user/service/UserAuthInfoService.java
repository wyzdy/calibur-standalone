package com.wyz.calibur.domain.user.service;

import com.wyz.calibur.pojo.user.UserBaseInfo;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * <p>
 * 用户验证 服务类
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
public interface UserAuthInfoService extends UserDetailsService {

    /**
     * 登录
     *
     * @param
     * @return
     * @author wei yz
     */
    String login(UserBaseInfo userBaseInfo);

}
