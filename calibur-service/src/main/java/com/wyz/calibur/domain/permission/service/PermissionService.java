package com.wyz.calibur.domain.permission.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wyz.calibur.pojo.permission.Permission;
import com.wyz.calibur.vo.base.MenuPermission;

import java.util.List;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
public interface PermissionService extends IService<Permission> {

    /**
     * 查询用户权限信息
     * @author wei yz
     * @param
     * @return
     */
    List<Permission> selectUserPermissionsByUserId(String userId);

    /**
     * 查询所有的菜单角色权限
     *
     * @param
     * @return
     * @author wei yz
     */
    List<MenuPermission> getAllMenuInfo();

}
