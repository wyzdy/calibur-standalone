package com.wyz.calibur.domain.role.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wyz.calibur.infrastructure.db.role.mapper.RoleMapper;
import com.wyz.calibur.pojo.role.Role;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
