package com.wyz.calibur.domain.role.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wyz.calibur.pojo.role.RolePermissionRel;

/**
 * <p>
 * 用户角色关系表 服务类
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
public interface RolePermissionRelService extends IService<RolePermissionRel> {

}
