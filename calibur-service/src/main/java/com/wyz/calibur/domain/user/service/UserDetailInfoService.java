package com.wyz.calibur.domain.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wyz.calibur.pojo.user.UserDetailInfo;

/**
 * <p>
 * 用户详细信息表 服务类
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
public interface UserDetailInfoService extends IService<UserDetailInfo> {

}
