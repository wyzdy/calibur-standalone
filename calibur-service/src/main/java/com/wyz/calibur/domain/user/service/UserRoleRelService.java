package com.wyz.calibur.domain.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wyz.calibur.pojo.user.UserRoleRel;

/**
 * <p>
 * 用户角色关系表 服务类
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
public interface UserRoleRelService extends IService<UserRoleRel> {

}
