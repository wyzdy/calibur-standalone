package com.wyz.calibur.domain.permission.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wyz.calibur.infrastructure.db.permission.mapper.PermissionMapper;
import com.wyz.calibur.pojo.permission.Permission;
import com.wyz.calibur.vo.base.MenuPermission;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

    @Override
    public List<Permission> selectUserPermissionsByUserId(String userId) {
        QueryWrapper<Permission> wrapper = Wrappers.<Permission>query();
        wrapper.eq("urr.user_id", userId);
        return baseMapper.selectUserPermissionsByUserId(wrapper);
    }

    @Override
    public List<MenuPermission> getAllMenuInfo() {
        return baseMapper.getAllMenuInfo(
                Wrappers.<Permission>query()
                        .eq("r.deleted", 0)
                        .eq("rpr.deleted", 0)
                        .eq("per.deleted", 0)
        );
    }
}
