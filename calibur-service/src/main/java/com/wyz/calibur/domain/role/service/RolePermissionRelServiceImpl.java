package com.wyz.calibur.domain.role.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wyz.calibur.infrastructure.db.role.mapper.RolePermissionRelMapper;
import com.wyz.calibur.pojo.role.RolePermissionRel;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色关系表 服务实现类
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
@Service
public class RolePermissionRelServiceImpl extends ServiceImpl<RolePermissionRelMapper, RolePermissionRel> implements RolePermissionRelService {

}
