package com.wyz.calibur.domain.user.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wyz.calibur.infrastructure.db.user.mapper.UserDetailInfoMapper;
import com.wyz.calibur.pojo.user.UserDetailInfo;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户详细信息表 服务实现类
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
@Service
public class UserDetailInfoServiceImpl extends ServiceImpl<UserDetailInfoMapper, UserDetailInfo> implements UserDetailInfoService {

}
