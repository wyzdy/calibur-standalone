package com.wyz.calibur.domain.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wyz.calibur.pojo.user.UserBaseInfo;

/**
 * <p>
 * 用户基础信息表 服务类
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
public interface UserBaseInfoService extends IService<UserBaseInfo> {



}
