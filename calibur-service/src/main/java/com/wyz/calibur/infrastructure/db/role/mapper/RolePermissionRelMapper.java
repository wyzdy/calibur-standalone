package com.wyz.calibur.infrastructure.db.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyz.calibur.pojo.role.RolePermissionRel;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户角色关系表 Mapper 接口
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
@Repository
public interface RolePermissionRelMapper extends BaseMapper<RolePermissionRel> {

}
