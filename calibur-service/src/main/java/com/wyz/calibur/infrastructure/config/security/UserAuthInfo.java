package com.wyz.calibur.infrastructure.config.security;

import com.wyz.calibur.pojo.role.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户基础信息表
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
public class UserAuthInfo implements Serializable, UserDetails {

    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private String id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 是否激活
     */
    private boolean enabled;
    /**
     * 锁
     */
    private boolean locked;
    /**
     * 角色
     */
    private List<Role> roles;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles.stream()
                .map(r -> new SimpleGrantedAuthority(r.getCode()))
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getId() {
        return id;
    }

    public boolean isLocked() {
        return true;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public UserAuthInfo setId(String id) {
        this.id = id;
        return this;
    }

    public UserAuthInfo setUsername(String username) {
        this.username = username;
        return this;
    }

    public UserAuthInfo setPassword(String password) {
        this.password = password;
        return this;
    }

    public UserAuthInfo setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public UserAuthInfo setLocked(boolean locked) {
        this.locked = locked;
        return this;
    }

    public UserAuthInfo setRoles(List<Role> roles) {
        this.roles = roles;
        return this;
    }

    @Override
    public String toString() {
        return "UserAuthInfo{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", enabled=" + enabled +
                ", locked=" + locked +
                ", roles=" + roles +
                '}';
    }

}
