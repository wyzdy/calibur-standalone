package com.wyz.calibur.infrastructure.config.security;

import com.alibaba.fastjson.JSON;
import com.wyz.calibur.constant.SecurityConstant;
import com.wyz.calibur.enums.ResultCode;
import com.wyz.calibur.result.Result;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Description: 退出成功
 * @Author: wei yz
 * @Date: 2022/6/23 14:11
 */
public class CliburLogoutSuccessHandler implements LogoutSuccessHandler {

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setContentType(SecurityConstant.CONTENT_TYPE);
        response.getWriter().println(
                JSON.toJSONString(new Result()
                        .setSuccess(true)
                        .setCode(ResultCode.SUCCESS)
                        .setData(null)
                        .setMessage(ResultCode.SUCCESS.getDesc())));
    }

}
