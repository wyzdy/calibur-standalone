package com.wyz.calibur.infrastructure.utils;

import cn.hutool.crypto.SecureUtil;
import com.wyz.calibur.enums.RedisKeyEnum;
import com.wyz.calibur.infrastructure.config.security.UserAuthInfo;
import com.wyz.calibur.infrastructure.config.security.UserAuthInfoDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @Description:
 * @Author: wei yz
 * @Date: 2022/6/23 14:49
 */
@Component
public class AuthenticationUtil {

    private static final Logger log = LoggerFactory.getLogger(AuthenticationUtil.class);

    @Autowired
    private RedisUtil redisUtil;

    /**
     * @param
     * @return
     * @author wei yz
     */
    public String getFrontToken(UserDetails userDetails) {
        UserAuthInfo userAuthInfo = (UserAuthInfo) userDetails;
        UserAuthInfoDTO userAuthInfoDTO = new UserAuthInfoDTO();
        BeanUtils.copyProperties(userAuthInfo, userAuthInfoDTO);
        // 生成客户端 client token
        String key = Utils.lowerCaseUUID();
        // 存到redis的用户名 ==> 创建用户登录凭证 ==> server token
        String jwtUsername = JWTTokenUtil.create(userDetails.getUsername());
        log.info("==> 返回前端的token: {}", key);
        log.info("==> 存到redis的用户名: {}", jwtUsername);
        log.info("==> 存到redis的用户信息: {}", userAuthInfoDTO);
        // token信息
        redisUtil.set(RedisKeyEnum.AUTH_TOKEN.getKey() + key, jwtUsername);
        // 用户信息
        redisUtil.set(RedisKeyEnum.USER_INFO_BASE.getKey() + key, userAuthInfoDTO);
        // 登录凭证
        redisUtil.set(RedisKeyEnum.LOGIN_VOUCHER.getKey() + SecureUtil.md5(userDetails.getUsername()), key);
        return key;
    }

    /**
     * 删除token信息
     *
     * @param tokenKey   客户端token
     * @param voucherKey 登录凭证key
     * @return
     * @author wei yz
     */
    public void delTokenInfo(String tokenKey, String voucherKey) {
        // 删除认证信息
        redisUtil.del(RedisKeyEnum.AUTH_TOKEN.getKey() + tokenKey);
        // 删除登录凭证信息
        redisUtil.del(voucherKey);
        // 删除登录者信息
        redisUtil.del(RedisKeyEnum.USER_INFO_BASE.getKey() + tokenKey);
    }

    /**
     * 删除所有token信息
     *
     * @return
     * @author wei yz
     */
    public void delTokenInfo() {
        redisUtil.del(Arrays.asList(RedisKeyEnum.AUTH_TOKEN.getKey() + "*",
                RedisKeyEnum.LOGIN_VOUCHER.getKey() + "*",
                RedisKeyEnum.USER_INFO_BASE.getKey() + "*"));
    }

    /**
     * 刷新token
     *
     * @param
     * @return
     * @author wei yz
     */
    public void refreshToken(UserDetails userDetails, String tokenKey, String cliToken) throws Exception {
        // server token
        String token = JWTTokenUtil.refresh(cliToken);
        // token信息
        redisUtil.set(RedisKeyEnum.AUTH_TOKEN.getKey() + tokenKey, token);
        // 用户信息
        redisUtil.set(RedisKeyEnum.USER_INFO_BASE.getKey() + tokenKey, userDetails);
    }

}
