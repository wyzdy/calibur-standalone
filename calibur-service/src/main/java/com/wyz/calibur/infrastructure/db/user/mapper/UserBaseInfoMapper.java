package com.wyz.calibur.infrastructure.db.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyz.calibur.pojo.user.UserBaseInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户基础信息表 Mapper 接口
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
@Repository
public interface UserBaseInfoMapper extends BaseMapper<UserBaseInfo> {

    /**
     * 根据用户名查询用户信息
     * @author wei yz
     * @param
     * @return
     */
    UserBaseInfo loadUserByUsername(@Param("username") String username);

}
