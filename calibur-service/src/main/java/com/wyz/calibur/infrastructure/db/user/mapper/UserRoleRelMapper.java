package com.wyz.calibur.infrastructure.db.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyz.calibur.pojo.user.UserRoleRel;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户角色关系表 Mapper 接口
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
@Repository
public interface UserRoleRelMapper extends BaseMapper<UserRoleRel> {

}
