package com.wyz.calibur.infrastructure.config.security;

import com.wyz.calibur.pojo.role.Role;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 用户基础信息表
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
public class UserAuthInfoDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private String id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 是否激活
     */
    private boolean enabled;
    /**
     * 锁
     */
    private boolean locked;
    /**
     * 角色
     */
    private List<Role> roles;

    @Override
    public String toString() {
        return "UserAuthInfoDTO{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", enabled=" + enabled +
                ", locked=" + locked +
                ", roles=" + roles +
                '}';
    }

    public UserAuthInfoDTO() {
    }

    public UserAuthInfoDTO(String id, String username, String password, boolean enabled, boolean locked, List<Role> roles) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.locked = locked;
        this.roles = roles;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
