package com.wyz.calibur.infrastructure.db.permission.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.wyz.calibur.pojo.permission.Permission;
import com.wyz.calibur.vo.base.MenuPermission;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
@Repository
public interface PermissionMapper extends BaseMapper<Permission> {

    /**
     * 查询用户权限信息
     *
     * @param
     * @return
     * @author wei yz
     */
    List<Permission> selectUserPermissionsByUserId(@Param(Constants.WRAPPER) Wrapper<Permission> wrapper);

    /**
     * 查询所有的菜单角色权限
     *
     * @param
     * @return
     * @author wei yz
     */
    List<MenuPermission> getAllMenuInfo(@Param(Constants.WRAPPER) Wrapper<Permission> wrapper);
}
