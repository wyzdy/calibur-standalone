package com.wyz.calibur.infrastructure.config.security;

import com.alibaba.fastjson.JSON;
import com.wyz.calibur.constant.SecurityConstant;
import com.wyz.calibur.enums.ResultCode;
import com.wyz.calibur.result.Result;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Description: 授权异常
 * @Author: wei yz
 * @Date: 2022/6/20 15:35
 */
public class CliburAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
        response.setStatus(ResultCode.PARAMETER_ERROR.getCode());
        response.setContentType(SecurityConstant.CONTENT_TYPE);
        response.getWriter().println(
                JSON.toJSONString(new Result()
                        .setSuccess(false)
                        .setCode(ResultCode.PARAMETER_ERROR)
                        .setMessage(ResultCode.PARAMETER_ERROR.getDesc())));
    }


}
