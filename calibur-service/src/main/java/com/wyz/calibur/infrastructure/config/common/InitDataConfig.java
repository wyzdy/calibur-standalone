package com.wyz.calibur.infrastructure.config.common;

import com.wyz.calibur.domain.permission.service.PermissionService;
import com.wyz.calibur.enums.RedisKeyEnum;
import com.wyz.calibur.infrastructure.utils.AuthenticationUtil;
import com.wyz.calibur.infrastructure.utils.RedisUtil;
import com.wyz.calibur.vo.base.MenuPermission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @Description: 初始化数据
 * @Author: wei yz
 * @Date: 2022/6/24 10:07
 */
@Order
@Component
public class InitDataConfig implements InitializingBean {
    private static final Logger log = LoggerFactory.getLogger(InitDataConfig.class);


    @Autowired
    private AuthenticationUtil authenticationUtil;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private RedisUtil redisUtil;

    @Override
    public void afterPropertiesSet() throws Exception {
        // 清除用户数据
        authenticationUtil.delTokenInfo();
        log.info("=========清除历史数据=========");
        // 查询菜单权限
        List<MenuPermission> allMenuInfo = permissionService.getAllMenuInfo();
        if (!CollectionUtils.isEmpty(allMenuInfo)) {
            redisUtil.set(RedisKeyEnum.MENU_PERMISSION.getKey(), allMenuInfo, 0L);
            log.info("==> 初始化菜单权限数据： {}", allMenuInfo);
        }
    }
}
