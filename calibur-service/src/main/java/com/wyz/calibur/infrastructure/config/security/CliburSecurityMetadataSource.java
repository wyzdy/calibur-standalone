package com.wyz.calibur.infrastructure.config.security;

import cn.hutool.core.bean.BeanUtil;
import com.wyz.calibur.constant.AuthConstant;
import com.wyz.calibur.domain.permission.service.PermissionService;
import com.wyz.calibur.enums.RedisKeyEnum;
import com.wyz.calibur.infrastructure.utils.RedisUtil;
import com.wyz.calibur.infrastructure.utils.SpringUtil;
import com.wyz.calibur.pojo.role.Role;
import com.wyz.calibur.vo.base.MenuPermission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.PathContainer;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.util.CollectionUtils;
import org.springframework.web.util.pattern.PathPatternParser;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 自定义的Metadata，通过当前请求地址，获取该地址所需要的角色 权限查询器
 * @Author: wei yz
 * @Date: 2022/6/18 15:31
 */
public class CliburSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {
    private static final Logger log = LoggerFactory.getLogger(CliburSecurityMetadataSource.class);

    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private PermissionService permissionService;

    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        log.info("==>权限查询器");
        // object 中包含用户请求的 request 信息
        HttpServletRequest request = ((FilterInvocation) object).getRequest();
        // 提取出当前的uri
        String requestURI = request.getRequestURI();
        log.info("==> 当前请求的URI：{}", requestURI);
        if (BeanUtil.isEmpty(redisUtil)) {
            redisUtil = SpringUtil.getBean(RedisUtil.class);
        }
        // 查询redis获取所有菜单信息
        List<MenuPermission> permissionInfoList = (List<MenuPermission>) redisUtil.get(RedisKeyEnum.MENU_PERMISSION.getKey());
        // 不存在则查询数据库
        if (CollectionUtils.isEmpty(permissionInfoList)) {
            permissionInfoList = permissionService.getAllMenuInfo();
            redisUtil.set(RedisKeyEnum.MENU_PERMISSION.getKey(), permissionInfoList);
        }
        for (MenuPermission menu : permissionInfoList) {
            // 匹配规则正确
            if (new PathPatternParser().parse(requestURI).matches(PathContainer.parsePath(menu.getUrl())) && !CollectionUtils.isEmpty(menu.getRoles())) {
                String[] roleCodes = menu.getRoles().stream().map(Role::getCode).toArray(String[]::new);
                return SecurityConfig.createList(roleCodes);
            }
        }
        return SecurityConfig.createList(AuthConstant.ROLE_LOGIN);
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return FilterInvocation.class.isAssignableFrom(aClass);
    }

}
