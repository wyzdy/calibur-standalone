package com.wyz.calibur.infrastructure.db.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyz.calibur.pojo.user.UserDetailInfo;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户详细信息表 Mapper 接口
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
@Repository
public interface UserDetailInfoMapper extends BaseMapper<UserDetailInfo> {

}
