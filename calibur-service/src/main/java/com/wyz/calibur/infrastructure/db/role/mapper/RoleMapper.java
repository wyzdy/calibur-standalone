package com.wyz.calibur.infrastructure.db.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyz.calibur.pojo.role.Role;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author Wei yz
 * @since 2022-01-19
 */
@Repository
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 根据用户ID查询角色信息列表
     *
     * @param
     * @return
     * @author wei yz
     */
    List<Role> getRoleListByUserId(@Param("userId") String uid);
}
