package com.wyz.calibur.infrastructure.utils;

import java.util.UUID;

/**
 * @author Wei yz
 * @ClassName: Utils
 * @Description:
 * @date 2022/1/19 22:16
 */
public class Utils {
    /**
     * 生成32位UUID大写字母
     */
    public static String upperCaseUUID() {
        // 转换大写
        return UUID.randomUUID().toString().replace("-", "").toUpperCase();
    }
    /**
     * 生成32位UUID小写字母
     */
    public static String lowerCaseUUID() {
        return UUID.randomUUID().toString().replace("-", "").toLowerCase();
    }

    public static void main(String[] args) {
        String uuid = upperCaseUUID();
        System.out.println("创建的ID：" + uuid);
    }

}
