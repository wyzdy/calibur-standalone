package com.wyz.calibur;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Description:
 * @Author: wei yz
 * @Date: 2022/7/9 13:45
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan({"com.wyz.calibur.infrastructure.db.*.mapper"})
@ComponentScan(basePackages = {"com.wyz.calibur.**"})
public class CaliburApplication {

    public static void main(String[] args) {
        SpringApplication.run(CaliburApplication.class, args);
    }

}