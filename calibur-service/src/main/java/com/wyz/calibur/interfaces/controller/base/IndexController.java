package com.wyz.calibur.interfaces.controller.base;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.wyz.calibur.enums.ResultCode;
import com.wyz.calibur.result.Result;
import com.wyz.calibur.result.ResultGenerator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: wei yz
 * @Date: 2022/7/9 16:28
 */
@Api(tags = "主页模块", value = "主页模块")
@RequestMapping("/index")
@RestController
public class IndexController {

    private static final Logger log = LoggerFactory.getLogger(IndexController.class);


    @GetMapping("/info")
    @ApiImplicitParam(name = "name", value = "测试", required = true)
    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "输入用户名查询")
    public ResponseEntity<String> sayHi(@RequestParam(value = "name") String name) {
        return ResponseEntity.ok("Hi:" + name);
    }

    @GetMapping("/error")
    public Result error() {
        log.error("===========错误页面===========");
        return ResultGenerator.genFailResult(ResultCode.INTERNAL_SERVER_ERROR, "错误页");
    }

    @GetMapping("/index")
    public Result index() {
        log.info("============主页============");
        return ResultGenerator.genSuccessResult("主页");
    }
}