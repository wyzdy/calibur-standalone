package com.wyz.calibur.interfaces.controller.user;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.wyz.calibur.infrastructure.utils.ContextUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: wei yz
 * @Date: 2022/7/9 14:41
 */
@Api(tags = "用户模块", value = "用户模块")
@RequestMapping("/user")
@RestController
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private ContextUtil contextUtil;


    @GetMapping("/info")
    @ApiImplicitParam(name = "name", value = "测试", required = true)
    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "输入用户名查询")
    public ResponseEntity<String> sayHi(@RequestParam(value = "name") String name) {
        return ResponseEntity.ok("Hi:" + name);
    }

    @GetMapping("/admin/hello")
    public String admin() {
        log.info("管理员接口：/userInfo/admin/hello");
        return "管理员接口：/userInfo/admin/hello";
    }

    @GetMapping("/user/hello")
    public String user() {
        log.info("普通用户接口：/userInfo/user/hello");

        contextUtil.currentUser();

        return "普通用户接口：/userInfo/user/hello";
    }

    @GetMapping("/guest/hello")
    public String guest() {
        log.info("游客接口：/userInfo/guest/hello");
        return "游客接口：/userInfo/guest/hello";
    }

    @GetMapping("/hello")
    public String hello() {
        log.info("普通接口：/userInfo/hello");
        return "普通接口：/userInfo/hello";
    }
}
