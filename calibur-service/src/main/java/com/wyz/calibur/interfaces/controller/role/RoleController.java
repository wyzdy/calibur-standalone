package com.wyz.calibur.interfaces.controller.role;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: wei yz
 * @Date: 2022/7/9 14:41
 */
@Api(tags = "角色模块", value = "角色模块")
@RequestMapping("/role")
@RestController
public class RoleController {

    @GetMapping("/info")
    @ApiImplicitParam(name = "name", value = "测试", required = true)
    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "输入角色名查询")
    public ResponseEntity<String> sayHi(@RequestParam(value = "name") String name) {
        return ResponseEntity.ok("Hi:" + name);
    }
}
