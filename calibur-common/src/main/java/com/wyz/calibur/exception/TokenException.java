package com.wyz.calibur.exception;

/**
 * @Description: token 异常
 * @Author: wei yz
 * @Date: 2022/7/17 14:51
 */
public class TokenException extends Exception {

    private static final long serialVersionUID = 1L;

    public TokenException(String mesaage) {
        super(mesaage);
    }
}
