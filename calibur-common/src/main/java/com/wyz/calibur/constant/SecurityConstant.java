package com.wyz.calibur.constant;

/**
 * @Description: 安全常量类
 * @author: wei yz
 * @Date: 2022/7/17
 */
public interface SecurityConstant {
    /**
     * 账号
     */
    String USERNAME = "username";
    /**
     * 密码
     */
    String PASSWORD = "password";
    /**
     * 登录的接口
     */
    String LOGIN_URL = "/login";
    /**
     * 登出的接口
     */
    String LOGOUT_URL = "/logout";
    /**
     * 上下文类型
     */
    String CONTENT_TYPE="application/json;charset=UTF-8";
    /**
     * TOKEN
     */
    String TOKEN="token";
}
