package com.wyz.calibur.constant;

/**
 * @Description: 认证常量类
 * @author: wei yz
 * @Date: 2022/7/17
 */
public interface AuthConstant {
    /**
     * token key
     */
    String TOKEN_HEADER = "Authorization";
    /**
     * token 拼接字段
     */
    String TOKEN_HEAD = "Bearer ";
    /**
     * 默认角色
     */
    String ROLE_LOGIN = "ROLE_LOGIN";
}
