package com.wyz.calibur.pojo.user;

import com.wyz.calibur.pojo.base.BaseModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 用户基础信息类
 * @Author: wei yz
 * @Date: 2022/7/17 14:35
 */
public class UserBaseInfo extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String username;
    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;
    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    private String sex;
    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;
    /**
     * 地址
     */
    @ApiModelProperty(value = "地址")
    private String address;
    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String phone;

    public String getUsername() {
        return username;
    }

    public UserBaseInfo setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserBaseInfo setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getSex() {
        return sex;
    }

    public UserBaseInfo setSex(String sex) {
        this.sex = sex;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserBaseInfo setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public UserBaseInfo setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public UserBaseInfo setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    @Override
    public String toString() {
        return "UserBaseInfo{" +
                "username=" + username +
                ", password=" + password +
                ", sex=" + sex +
                ", email=" + email +
                ", address=" + address +
                ", phone=" + phone +
                "}";
    }
}

