package com.wyz.calibur.pojo.role;

import com.wyz.calibur.pojo.base.BaseModel;

/**
 * @Description: 角色类
 * @Author: wei yz
 * @Date: 2022/7/17 14:19
 */
public class Role extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;
    /**
     * 编码
     */
    private String code;

    public String getName() {
        return name;
    }

    public Role setName(String name) {
        this.name = name;
        return this;
    }

    public String getCode() {
        return code;
    }

    public Role setCode(String code) {
        this.code = code;
        return this;
    }

    @Override
    public String toString() {
        return "Role{" +
                "name='" + name + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
