package com.wyz.calibur.pojo.user;

import com.wyz.calibur.pojo.base.BaseModel;

/**
 * @Description: 用户明细信息类
 * @Author: wei yz
 * @Date: 2022/7/17 14:39
 */
public class UserDetailInfo extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 基础信息ID
     */
    private String parentId;

    public String getParentId() {
        return parentId;
    }

    public UserDetailInfo setParentId(String parentId) {
        this.parentId = parentId;
        return this;
    }

    @Override
    public String toString() {
        return "UserDetailInfo{" +
                "parentId=" + parentId +
                "}";
    }
}

