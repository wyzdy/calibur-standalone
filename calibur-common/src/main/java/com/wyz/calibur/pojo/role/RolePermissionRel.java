package com.wyz.calibur.pojo.role;

import com.wyz.calibur.pojo.base.BaseModel;

/**
 * @Description: 角色权限关系类
 * @Author: wei yz
 * @Date: 2022/7/17 14:41
 */
public class RolePermissionRel extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 角色信息id
     */
    private String roleId;

    /**
     * 权限信息id
     */
    private String permissionId;

    public String getRoleId() {
        return roleId;
    }

    public RolePermissionRel setRoleId(String roleId) {
        this.roleId = roleId;
        return this;
    }

    public String getPermissionId() {
        return permissionId;
    }

    public RolePermissionRel setPermissionId(String permissionId) {
        this.permissionId = permissionId;
        return this;
    }

    @Override
    public String toString() {
        return "RolePermissionRel{" +
                "roleId=" + roleId +
                ", permissionId=" + permissionId +
                "}";
    }
}
