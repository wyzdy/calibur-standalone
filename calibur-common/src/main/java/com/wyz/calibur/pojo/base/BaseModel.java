package com.wyz.calibur.pojo.base;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * @Description:
 * @Author: wei yz
 * @Date: 2022/7/17 14:11
 */
public class BaseModel {
    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "主键ID")
    private String id;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;
    /**
     * 版本
     */
    @ApiModelProperty(value = "版本")
    @TableField(fill = FieldFill.INSERT)
    private Integer version;
    /**
     * 逻辑删除 0 未删除 1已删除
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT, select = false)
    private Integer deleted;

    public String getId() {
        return id;
    }

    public BaseModel setId(String id) {
        this.id = id;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public BaseModel setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public BaseModel setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public Integer getVersion() {
        return version;
    }

    public BaseModel setVersion(Integer version) {
        this.version = version;
        return this;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public BaseModel setDeleted(Integer deleted) {
        this.deleted = deleted;
        return this;
    }
}
