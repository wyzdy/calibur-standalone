package com.wyz.calibur.pojo.permission;

import com.wyz.calibur.pojo.base.BaseModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description:
 * @Author: wei yz
 * @Date: 2022/7/17 14:38
 */
public class Permission extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    @ApiModelProperty(value = "权限名称")
    private String name;
    /**
     * url
     */
    @ApiModelProperty(value = "URL")
    private String url;
    /**
     * 类型 0 菜单 1权限
     */
    @ApiModelProperty(value = "类型 0 菜单 1权限")
    private String type;
    /**
     * 父ID
     */
    @ApiModelProperty(value = "父ID")
    private String parentId;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Long sort;

    public String getName() {
        return name;
    }

    public Permission setName(String name) {
        this.name = name;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public Permission setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getType() {
        return type;
    }

    public Permission setType(String type) {
        this.type = type;
        return this;
    }

    public String getParentId() {
        return parentId;
    }

    public Permission setParentId(String parentId) {
        this.parentId = parentId;
        return this;
    }

    public Long getSort() {
        return sort;
    }

    public Permission setSort(Long sort) {
        this.sort = sort;
        return this;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "name=" + name +
                ", url=" + url +
                ", type=" + type +
                ", parentId=" + parentId +
                ", sort=" + sort +
                "}";
    }
}

