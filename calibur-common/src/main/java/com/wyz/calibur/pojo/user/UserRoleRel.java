package com.wyz.calibur.pojo.user;

import com.wyz.calibur.pojo.base.BaseModel;

/**
 * @Description: 用户角色关系类
 * @Author: wei yz
 * @Date: 2022/7/17 14:40
 */
public class UserRoleRel extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 用户信息id
     */
    private String userId;

    /**
     * 角色信息id
     */
    private String roleId;

    public String getUserId() {
        return userId;
    }

    public UserRoleRel setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getRoleId() {
        return roleId;
    }

    public UserRoleRel setRoleId(String roleId) {
        this.roleId = roleId;
        return this;
    }

    @Override
    public String toString() {
        return "UserRoleRel{" +
                "userId=" + userId +
                ", roleId=" + roleId +
                "}";
    }
}
