package com.wyz.calibur.result;

import com.wyz.calibur.enums.ResultCode;

/**
 * @Description: 统一返回类型实体
 * @Author: wei yz
 * @Date: 2022/7/17 14:48
 */
public class Result {
    private int code;
    private String message;
    private Object data;
    private Boolean success = true;

    public Result setCode(ResultCode resultCode) {
        this.code = resultCode.code;
        return this;
    }

    public int getCode() {
        return code;
    }

    public Result setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Result setMessage(String message) {
        this.message = message;
        return this;
    }

    public Object getData() {
        return data;
    }

    public Result setData(Object data) {
        this.data = data;
        return this;
    }

    public Boolean getSuccess() {
        return success;
    }

    public Result setSuccess(Boolean success) {
        this.success = success;
        return this;
    }
}
