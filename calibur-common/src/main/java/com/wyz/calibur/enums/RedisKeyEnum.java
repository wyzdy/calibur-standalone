package com.wyz.calibur.enums;

/**
 * @Description: redis key 枚举类
 * @author: wei yz
 * @Date: 2022/7/17
 */
public enum RedisKeyEnum {
    /**
     * 用户信息
     */
    USER_INFO_BASE("userInfo:base:"),
    /**
     * 用户列表
     */
    USER_INFO_BASE_LIST("userInfo:base:list:"),
    /**
     * 认证
     */
    AUTH_TOKEN("auth:token:"),
    /**
     * 登录凭证
     */
    LOGIN_VOUCHER("login:voucher:"),
    /**
     * 菜单权限
     */
    MENU_PERMISSION("NOTE:SECURITY:MENU:PERMISSION:ALL"),
    TEST_KEY("test");
    private String key;

    RedisKeyEnum(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
