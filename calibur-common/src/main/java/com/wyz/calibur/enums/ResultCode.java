package com.wyz.calibur.enums;

public enum ResultCode {
    /**
     * 成功
     */
    SUCCESS(200,"成功"),
    /**
     * 失败
     */
    FAIL(400,"失败"),
    /**
     * 未认证（签名错误）
     */
    UNAUTHORIZED(401,"未认证（签名错误）"),
    /**
     * 参数错误
     */
    PARAMETER_ERROR(403,"参数错误"),
    /**
     * 接口不存在
     */
    NOT_FOUND(404,"接口不存在"),
    /**
     * 服务器内部错误
     */
    INTERNAL_SERVER_ERROR(500,"服务器内部错误"),
    /**
     * 访问权限异常
     */
    ACCESS_PERMISSION_ERROR(300,"访问权限异常"),
    /**
     * 没有权限
     */
    NOT_PERMISSION(301,"没有权限");

    public int code;
    public String desc;

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    ResultCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
