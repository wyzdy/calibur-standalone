package com.wyz.calibur.vo.base;

import com.wyz.calibur.pojo.role.Role;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 菜单权限实体类
 * @Author: wei yz
 * @Date: 2022/7/17 14:10
 */
public class MenuPermission implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID")
    private String id;
    /**
     * 名称
     */
    @ApiModelProperty(value = "权限名称")
    private String name;
    /**
     * url
     */
    @ApiModelProperty(value = "URL")
    private String url;
    /**
     * 类型 0 菜单 1权限
     */
    @ApiModelProperty(value = "类型 0 菜单 1权限")
    private String type;
    /**
     * 父ID
     */
    @ApiModelProperty(value = "父ID")
    private String parentId;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Long sort;
    /**
     * 角色列表
     */
    private List<Role> roles;

    public String getId() {
        return id;
    }

    public MenuPermission setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public MenuPermission setName(String name) {
        this.name = name;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public MenuPermission setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getType() {
        return type;
    }

    public MenuPermission setType(String type) {
        this.type = type;
        return this;
    }

    public String getParentId() {
        return parentId;
    }

    public MenuPermission setParentId(String parentId) {
        this.parentId = parentId;
        return this;
    }

    public Long getSort() {
        return sort;
    }

    public MenuPermission setSort(Long sort) {
        this.sort = sort;
        return this;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public MenuPermission setRoles(List<Role> roles) {
        this.roles = roles;
        return this;
    }

    @Override
    public String toString() {
        return "MenuPermission{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", type='" + type + '\'' +
                ", parentId='" + parentId + '\'' +
                ", sort=" + sort +
                ", roles=" + roles +
                '}';
    }
}
